package pl.sda.collections.zadaniaObiektowe.map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        Map<Long, Student> map = new HashMap<>();

        Student jan = new Student(111, "Kowalski 1", "Marian");
        map.put(111L, jan);
        map.put(222L, new Student(222, "Kowalski 2", "Marian"));
        map.put(331L, new Student(331, "Kowalski 3", "Marian"));
        map.put(431L, new Student(431, "Kowalski 4", "Marian"));
//        map.put(121L, new Student(121, "Kowalski 5", "Marian"));
        map.put(121L, jan);
        map.put(555L, new Student(555, "Kowalski 6", "Marian"));
//        map.put(55L, new Student(55, "Kowalski 7", "Marian"));
        map.put(55L, jan);
        map.put(777L, new Student(777, "Kowalski 8", "Marian"));

        if (map.containsKey(111L)) {
            System.out.println("Zawiera");
            System.out.println("Student: " + map.get(111L));
        } else {
            System.out.println("Nie zawiera");
        }

        System.out.println("Ilość studentów: " + map.size());


        // wypisanie wszystkich kluczy
        for (Long kluczWMapie : map.keySet()) {
            System.out.println("Klucz: " + kluczWMapie);
        }

        // wypisanie wszystkich wartości
        for (Student student : map.values()) { // wypisujemy wszystkich studentów
            System.out.println(student);
        }

        // iterujemy zbiór wpisów/rekordów w mapie ( rekord to para klucz+wartość)
        for (Map.Entry<Long, Student> wpis : map.entrySet()) {
            System.out.println("Klucz: " + wpis.getKey() + ", wartość: " + wpis.getValue());
        }


        University university = new University();
        university.dodajStudenta(123L, "a", "b");
        university.dodajStudenta(123L, "c", "b");
        university.dodajStudenta(123L, "d", "b");
        university.dodajStudenta(123L, "e", "b");
        university.dodajStudenta(123L, "f", "b");
        university.dodajStudenta(123L, "g", "b");
        university.dodajStudenta(123L, "h", "b");

//        try {
//            university.pobierzStudenta(125L);
//        } catch (Student student) {
//            System.out.println("Przechwyciłem studenta:" + student);
//        }
        university.pobierzStudenta(111L);

    }
}
