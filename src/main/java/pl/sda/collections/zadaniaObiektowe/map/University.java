package pl.sda.collections.zadaniaObiektowe.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class University {
    private Map<Long, Student> studentMap = new HashMap<>();

    public void dodajStudenta(Long indeks, String imie, String nazwisko) {

        // sprawdzam czy podany indeks istnieje w mapie
        if (studentMap.containsKey(indeks)) {
            System.err.println("Student z takim indeksem już istnieje.");
            while (studentMap.containsKey(indeks)) {
                // jeśli istnieje indeks, to dokonuje inkrementacji
                indeks++;
            }
        }

        Student stworzony = new Student(indeks, nazwisko, imie);
        // umieszczenie studenta w mapie
        studentMap.put(stworzony.getIndeks(), stworzony);
    }

    public boolean czyStudentIstnieje(Long indeks) {
        return studentMap.containsKey(indeks);
    }

    public Optional<Student> pobierzStudenta(Long indeks) throws NoSuchStudentException {
        if (!studentMap.containsKey(indeks)) {
            throw new NoSuchStudentException();
        }
        return Optional.ofNullable(studentMap.get(indeks));
    }

    public int pobierzIlośćStudentów() {
        return studentMap.size();
    }

    public int pobierzUnikalnychStudentów() {
        // long rzutowany na int
        return (int) studentMap.values().stream().distinct().count();
    }

    public void printAllStudents() {
        studentMap.values().stream()
                .distinct() // usuwamy duplikaty
                .forEach(System.out::println); // wypisujemy
    }

}
