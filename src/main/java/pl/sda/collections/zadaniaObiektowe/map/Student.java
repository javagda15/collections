package pl.sda.collections.zadaniaObiektowe.map;

public class Student extends Throwable {
    private String  nazwisko, imie;
    private Long indeks;

    public Student(long indeks, String nazwisko, String imie) {
        this.indeks = indeks;
        this.nazwisko = nazwisko;
        this.imie = imie;
    }

    @Override
    public String toString() {
        return "Student{" +
                "nazwisko='" + nazwisko + '\'' +
                ", imie='" + imie + '\'' +
                ", indeks=" + indeks +
                '}';
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public Long getIndeks() {
        return indeks;
    }

    public void setIndeks(Long indeks) {
        this.indeks = indeks;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
}
