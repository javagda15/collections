package pl.sda.collections.zadaniaObiektowe.zad2;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class Poczekalnia {
    private PriorityQueue<Klient> kolejka;

    public Poczekalnia() {
        this.kolejka = new PriorityQueue<>(new Comparator<Klient>() {
            @Override
            public int compare(Klient o1, Klient o2) {
                if (o1.getPriorytet() && !o2.getPriorytet()) {
                    return -1;
                } else if (o2.getPriorytet() && !o1.getPriorytet()) {
                    return 1;
                }
                if (o1.getCzas().isBefore(o2.getCzas())) {
                    return -1;
                } else if (o2.getCzas().isBefore(o1.getCzas())) {
                    return 1;
                }
                return 0;
            }
        });
    }

    public Klient pobierzNastepnegoKlienta(){
        return kolejka.poll();
    }

    public void dodajKlienta(String imie, boolean czyPriorytet) {
        kolejka.add(new Klient(imie, LocalDateTime.now(), czyPriorytet));
    }

    public void wyczyscITestuj() {
        while (!kolejka.isEmpty()) {
            System.out.println(kolejka.poll());
        }
    }

    public static void main(String[] args) {
        Poczekalnia poczekalnia = new Poczekalnia();
        poczekalnia.dodajKlienta("Marian1", true);
        poczekalnia.dodajKlienta("Marian2", false);
        poczekalnia.dodajKlienta("Marian3", false);
        poczekalnia.dodajKlienta("Marian4", false);
        poczekalnia.dodajKlienta("Marian5", true);
        poczekalnia.dodajKlienta("Marian6", true);

        poczekalnia.wyczyscITestuj();

    }
}
