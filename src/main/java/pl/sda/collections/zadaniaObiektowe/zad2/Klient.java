package pl.sda.collections.zadaniaObiektowe.zad2;

import java.time.LocalDateTime;

public class Klient {
    private String imie;
    private LocalDateTime czas;
    private Boolean priorytet;

    public Klient(String imie, LocalDateTime czas, Boolean priorytet) {
        this.imie = imie;
        this.czas = czas;
        this.priorytet = priorytet;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public LocalDateTime getCzas() {
        return czas;
    }

    public void setCzas(LocalDateTime czas) {
        this.czas = czas;
    }

    public Boolean getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(Boolean priorytet) {
        this.priorytet = priorytet;
    }

    @Override
    public String toString() {
        return "Klient{" +
                "imie='" + imie + '\'' +
                ", czas=" + czas +
                ", priorytet=" + priorytet +
                '}';
    }
}
