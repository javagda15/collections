package pl.sda.collections.zadaniaObiektowe.zad2;

import pl.sda.collections.zadaniaObiektowe.zad1.PodatekProduktu;
import pl.sda.collections.zadaniaObiektowe.zad1.Produkt;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();

        // dodać
        queue.add(123); // dodanie na koniec
        queue.add(124); // dodanie na koniec
        queue.add(125); // dodanie na koniec
        queue.add(126); // dodanie na koniec
        Integer element = queue.peek(); // zwróć ale nie usuwaj
        Integer usuniety = queue.poll(); // zwróć i usuń
        System.out.println(usuniety);


        PriorityQueue<Produkt> kolejkaProduktów = new PriorityQueue<>(
                new Comparator<Produkt>() {
                    @Override
                    public int compare(Produkt o1, Produkt o2) {
                        return Double.compare(o2.getCenaNetto(), o1.getCenaNetto());
                    }
                }
        );

        LocalDateTime dataICzas = LocalDateTime.now();
        LocalDateTime dataICzas2 = LocalDateTime.now();
        if (dataICzas.isEqual(dataICzas2)) {
            System.out.println("Czas pierwszy był przed czasem drugim");
        }

        kolejkaProduktów.add(new Produkt(PodatekProduktu.NO_VAT, "Masło", 2.30));
        kolejkaProduktów.add(new Produkt(PodatekProduktu.NO_VAT, "Chleb", 2.20));
        kolejkaProduktów.add(new Produkt(PodatekProduktu.NO_VAT, "Mąka", 5.40));
        kolejkaProduktów.add(new Produkt(PodatekProduktu.NO_VAT, "Żelki", 6.40));
        kolejkaProduktów.add(new Produkt(PodatekProduktu.NO_VAT, "Paluszki", 3.50));

//        System.out.println(kolejkaProduktów.peek());

        for (Produkt p :
                kolejkaProduktów) {
            System.out.println(p);
        }

    }
}
