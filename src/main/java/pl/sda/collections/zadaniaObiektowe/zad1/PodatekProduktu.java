package pl.sda.collections.zadaniaObiektowe.zad1;

public enum PodatekProduktu {
    VAT_8(8),
    VAT_23(23),
    VAT_5(5),
    NO_VAT(0);

    private int iloscPodatku;

    PodatekProduktu(int iloscPodatku) {
        this.iloscPodatku = iloscPodatku;
    }

    public int getIloscPodatku() {
        return iloscPodatku;
    }
}
