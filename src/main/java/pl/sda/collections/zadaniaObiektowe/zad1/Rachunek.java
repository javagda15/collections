package pl.sda.collections.zadaniaObiektowe.zad1;

import java.util.ArrayList;
import java.util.List;

public class Rachunek {
    private List<Produkt> produktList = new ArrayList<>();

    public Rachunek() {
    }

    public List<Produkt> getProduktList() {
        return produktList;
    }

    public void setProduktList(List<Produkt> produktList) {
        this.produktList = produktList;
    }

    public void wypiszWszystkieProdukty() {
        produktList.forEach(produkt -> System.out.println(produkt));
    }

    public double podsumujRachunekNetto() {
        // opcja 1 - bez streamu
//        double suma = 0.0;
//        for (Produkt produkt : produktList) {
//            suma += produkt.getCenaNetto();
//        }
//        return suma;
        return produktList.stream().mapToDouble(p -> p.getCenaNetto()).sum();
    }

    public double podsumujRachunekBrutto() {
        // opcja 1 - bez streamu
//        double suma = 0.0;
//        for (Produkt produkt : produktList) {
//            suma += produkt.podajCeneBrutto();
//        }
//        return suma;
        return produktList.stream().mapToDouble(p -> p.podajCeneBrutto()).sum();
    }

    public double zwrocWartoscPodatku() {
        return podsumujRachunekBrutto() - podsumujRachunekNetto();
    }
}
