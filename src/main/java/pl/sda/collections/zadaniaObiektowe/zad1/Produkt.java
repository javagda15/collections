package pl.sda.collections.zadaniaObiektowe.zad1;

public class Produkt {
    private PodatekProduktu podatekProduktu;
    private String nazwaProduktu;
    private double cenaNetto;

    public Produkt(PodatekProduktu podatekProduktu, String nazwaProduktu, double cenaNetto) {
        this.podatekProduktu = podatekProduktu;
        this.nazwaProduktu = nazwaProduktu;
        this.cenaNetto = cenaNetto;
    }

    public PodatekProduktu getPodatekProduktu() {
        return podatekProduktu;
    }

    public void setPodatekProduktu(PodatekProduktu podatekProduktu) {
        this.podatekProduktu = podatekProduktu;
    }

    public String getNazwaProduktu() {
        return nazwaProduktu;
    }

    public void setNazwaProduktu(String nazwaProduktu) {
        this.nazwaProduktu = nazwaProduktu;
    }

    public double getCenaNetto() {
        return cenaNetto;
    }

    public void setCenaNetto(double cenaNetto) {
        this.cenaNetto = cenaNetto;
    }

    public double podajCeneBrutto() {
        double procentPodatku = podatekProduktu.getIloscPodatku() / 100.0;
        double brutto = cenaNetto + cenaNetto * procentPodatku;
        return brutto;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "podatekProduktu=" + podatekProduktu +
                ", nazwaProduktu='" + nazwaProduktu + '\'' +
                ", cenaNetto=" + cenaNetto +
                '}';
    }
}
