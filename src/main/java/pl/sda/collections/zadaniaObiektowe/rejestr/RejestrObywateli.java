package pl.sda.collections.zadaniaObiektowe.rejestr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RejestrObywateli {
    private Map<String, Obywatel> obywatelMap = new HashMap<>();

    public void dodajObywatela(String imie, String nazwisko, String pesel) {
        obywatelMap.put(pesel, new Obywatel(imie, nazwisko, pesel));
    }

    public List<Obywatel> znajdzUrodzonychW(int rok) { // 2 cyfry roku
        // Urodzeni w konkretnym roku
        List<Obywatel> zbiorUrPrzed = new ArrayList<>();

        String rokString = "" + rok; // to jest konwersja int na string
//        String rokString = String.valueOf(rok);
        for (Obywatel obywatel : obywatelMap.values()) {
            // jeśli pesel zaczyna się od podanego roku
            if (obywatel.getPesel().startsWith(rokString)) {
                // to dodaj obywatela do zbioru
                zbiorUrPrzed.add(obywatel);
            }
        }
        return zbiorUrPrzed;
    }

    public List<Obywatel> znajdzUrodzonychPrzed(int rok) { // 2 cyfry roku
        List<Obywatel> zbiorUrPrzed = new ArrayList<>();

        for (Obywatel obywatel : obywatelMap.values()) {
            // substring wycina ze stringa ciąg znaków od do.
            String dwieCyfryPeselu = obywatel.getPesel().substring(0, 2);

            int rokZPeselu = Integer.parseInt(dwieCyfryPeselu);

            if (rokZPeselu < rok) {
                zbiorUrPrzed.add(obywatel);
            }
        }
        return zbiorUrPrzed;
    }

    public List<Obywatel> znajdzUrodzonychPrzed8(int rok) {
        return obywatelMap.values().stream()
                .filter(o -> o.podajRokUrodzenia() < rok)
                .collect(Collectors.toList());
    }




}
