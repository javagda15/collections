package pl.sda.collections.zbiory;

import java.util.*;
import java.util.stream.Collectors;

public class Rozwiazania {
//    public static void main(String[] args) {
//        Set<Integer> set = new TreeSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
//        System.out.println(set.size());
//
//        for (Integer element : set) {
//            System.out.println(element);
//        }
//        // ^^^ spełnia takie samo zadanie
//        set.forEach(System.out::println);
//
//        //###############3
//        set.remove(12);
//        set.remove(10);
//        System.out.println(set.size());
//        System.out.println(set);
//
//
//    }

    public static boolean sprawdzDuplikaty(String tekst) {
        Set<Character> characterSet = new HashSet<>(); // tworzę zbiór do którego dodam znaki
        for (char character : tekst.toCharArray()) { // dodaje znaki
            characterSet.add(character);
        }

        // jeśli znaków w zbiorze jest tyle samo co w tekście
        // to znaczy że nie usunęło duplikatów
        return characterSet.size() == tekst.length();
    }

    public static boolean sprawdzDuplikaty2(String tekst) {
        // zamiana tekstu na literki
        List<Character> chars = tekst.chars() // int stream
                .mapToObj(e -> ((char) e)) // znak zamieniam na obiekt
                .collect(Collectors.toList()); // zebranie do listy

        Set<Character> charsSet = new HashSet<>(chars);
        // wstawienie liter do setu
        // usuwa duplikaty
        return chars.size() == charsSet.size();
    }

    public static void main(String[] args) {
        sprawdzSlowa("ala ma kota a kot ma ale ale ala to ala i ala ali nie równa");
    }

    public static void sprawdzSlowa(String zdanie) {
        // zamieniam całe zdanie na małe litery.
//        String tekstMalymiLiterami = zdanie.toLowerCase();
        String[] slowa = zdanie.split(" "); // elementy dodaj do listy/zbioru
        // split dzieli zdanie. W argumencie podajemy jaki tekst ma być punktem podziału

        Set<String> zbiorUnikalnychSlow = new TreeSet<>(Arrays.asList(slowa));
//        System.out.println(Arrays.toString(slowa));
//        for (String slowo : slowa) {
//            System.out.println(slowo);
//        }

        zbiorUnikalnychSlow.forEach(System.out::println);


        // wypisanie unikalnych słów

    }
}
