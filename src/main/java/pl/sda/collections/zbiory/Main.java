package pl.sda.collections.zbiory;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        // dodaje elementy listy do setu.
        Set<Integer> set = new HashSet<>(Arrays.asList(12, 23, 34, 44));

        Set<Integer> sets = new LinkedHashSet<>(); // zachowuje kolejność wstawiania
        Set<Integer> sett = new TreeSet<>(); // sortuje po kluczach

        // zamiana tekstu na literki
        List<Character> chars = "tekst"
                .chars()
                .mapToObj(e -> ((char) e))
                .collect(Collectors.toList()); // zebranie do listy

        Set<Character> charsSet = new HashSet<>(chars);
        // wstawienie liter do setu
        // usuwa duplikaty
        if(chars.size() != charsSet.size()){
            System.out.println("Są duplikaty");
        }

        Map<Character, Long> wystapienia = "tekst"
                .chars()
                .mapToObj(e -> ((char) e))
                .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));

        System.out.println(wystapienia);

    }
}
