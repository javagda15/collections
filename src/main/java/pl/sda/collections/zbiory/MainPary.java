package pl.sda.collections.zbiory;

import java.util.HashSet;
import java.util.Set;

public class MainPary {
    public static void main(String[] args) {
        Set<ParaLiczb> paraLiczbs = new HashSet<>();
        paraLiczbs.add(new ParaLiczb(1, 2));
        paraLiczbs.add(new ParaLiczb(2, 1));
        paraLiczbs.add(new ParaLiczb(1, 1));
        paraLiczbs.add(new ParaLiczb(1, 2));

        System.out.println(paraLiczbs);
    }
}
