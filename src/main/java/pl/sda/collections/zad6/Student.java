package pl.sda.collections.zad6;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private List<Double> oceny = new ArrayList<>();
    private String indeks;
    private String imie;
    private String nazwisko;

    public Student() {
    }

    public Student(String indeks, String imie, String nazwisko) {
        this.indeks = indeks;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Student(List<Double> oceny, String indeks, String imie, String nazwisko) {
        this.oceny = oceny;
        this.indeks = indeks;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Double obliczSrednia() {
        double suma = 0.0;
        for (Double ocena : oceny) {
            suma += ocena;
        }
        return suma / oceny.size();
    }

    public List<Double> getOceny() {
        return oceny;
    }

    public void setOceny(List<Double> oceny) {
        this.oceny = oceny;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return "Student{" +
                "oceny=" + oceny +
                ", indeks='" + indeks + '\'' +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }
}
