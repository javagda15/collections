package pl.sda.collections.zad6;


import java.util.*;

public class Dziennik {
    private List<Student> studentList = new ArrayList<>();

    /**
     * Dodanie studenta do listy / dziennika.
     *
     * @param student - student do dodania.
     */
    public void dodajStudenta(Student student) {
        studentList.add(student);
    }

    public void usunStudenta(Student student) {
        studentList.remove(student);
    }

    public void usunStudenta(String index) {
        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).getIndeks().equals(index)) {
                studentList.remove(studentList.get(i));
                i--;
            }
        }
    }
//
//    public Student zwrocStudenta(String index){
//        for (Student student : studentList) {
//            if (student.getIndeks().equals(index)) {
//                return student;
//            }
//        }
//        //?
//        return null;
//    }

    public Optional<Student> zwrocStudenta(String index) {
        for (Student student : studentList) {
            if (student.getIndeks().equals(index)) {
                return Optional.of(student);
            }
        }
        //?
        return Optional.empty();
    }

    public OptionalDouble podajSredniaStudenta(String index) {
        Optional<Student> studentOptional = zwrocStudenta(index);
        if (studentOptional.isPresent()) {
            double srednia = studentOptional.get().obliczSrednia();
            return OptionalDouble.of(srednia);
        }
        return OptionalDouble.empty();
    }

    public List<Student> podajZagrozonych() {
        List<Student> zagrozeni = new ArrayList<>();

        for (Student student : studentList) {
            if (student.obliczSrednia() <= 2.0) {
                zagrozeni.add(student);
            }
        }
        return zagrozeni;
    }

    public List<Student> posortujPoIndeksie() {
        List<Student> studenty = new ArrayList<>(studentList);

        studenty.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                // -1 gdy o1 wiekszy od o2
                // 1 gdy o1 mniejszy od o2
                // 0 gdy równe
//                if (o1.getIndeks().equals(o2.getIndeks())) {
//                    return 0;
//                }
                int indeks1 = Integer.parseInt(o1.getIndeks());
                int indeks2 = Integer.parseInt(o2.getIndeks());
                if (indeks1 > indeks2) {
                    return 1;
                }else if( indeks1 < indeks2) {
                    return -1;
                }
                return 0;
            }
        });

        return studenty;
    }
}
