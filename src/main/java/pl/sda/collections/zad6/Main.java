package pl.sda.collections.zad6;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Dziennik dziennik = new Dziennik();

        dziennik.dodajStudenta(new Student("123", "a", "b"));
        dziennik.dodajStudenta(new Student("123", "k", "c"));
        dziennik.dodajStudenta(new Student("124", "j", "d"));
        dziennik.dodajStudenta(new Student("125", "h", "e"));
        dziennik.dodajStudenta(new Student("123", "g", "f"));
        dziennik.dodajStudenta(new Student("143", "g", "f"));
        dziennik.dodajStudenta(new Student("193", "g", "f"));

//        dziennik.usunStudenta("123");

//        Student student = dziennik.zwrocStudenta("126");
//        if(student != null) {
//            System.out.println(student.getImie() + " " + student.getNazwisko());
//        }
        Optional<Student> student = dziennik.zwrocStudenta("126");
        if(student.isPresent()) {
            Student student1 = student.get();
            System.out.println(student1.getImie() + " " + student1.getNazwisko());
        }


        System.out.println(dziennik.posortujPoIndeksie());
    }
}
