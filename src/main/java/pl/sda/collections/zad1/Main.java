package pl.sda.collections.zad1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            int zmienna = scanner.nextInt();
            list.add(zmienna);

//            list.add(scanner.nextInt());
        }

        Random generator = new Random();
        for (int i = 0; i < 5; i++) {
            int zmienna = generator.nextInt(100);
            list.add(zmienna);

//            list.add(generator.nextInt());
        }

        System.out.println(list);

        Collections.sort(list);
    }
}
