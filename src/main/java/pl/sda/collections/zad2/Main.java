package pl.sda.collections.zad2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList();
        // wygenerowanie
        Random generator = new Random();
        for (int i = 0; i < 20; i++) {
            int zmienna = generator.nextInt(100);
            list.add(zmienna);
        }

        // suma
        int suma = 0;
        for (int i = 0; i < list.size(); i++) {
            suma += list.get(i);
        }
        System.out.println(suma);

        // średnia
        System.out.println(suma / list.size());

        List<Integer> kopia = new ArrayList<>(list);
        Collections.sort(kopia);

        // mediana
        double mediana = 0;
        if (kopia.size() % 2 == 0) {
            // srednia dwóch elementów
            mediana = (kopia.get((kopia.size() / 2) - 1) + kopia.get((kopia.size() / 2))) / 2.0;
        } else {
            mediana = kopia.get(kopia.size() / 2); // środkowy element
        }

        System.out.println(mediana);

        int min = list.get(0);
        int max = list.get(0);
        int minIndex = 0, maxIndex = 0;
        for (int i = 0; i < list.size(); i++) {
            if (min > list.get(i)) {
                min = list.get(i);
                minIndex = i;
            }
            if (max < list.get(i)) {
                max = list.get(i);
                maxIndex = i;
            }
        }
        System.out.println("min: " + min + " at " + minIndex);
        System.out.println("max: " + max + " at " + maxIndex);

        System.out.println("Index Min: " + list.indexOf(min));
        System.out.println("Index Max: " + list.indexOf(max));
    }
}
