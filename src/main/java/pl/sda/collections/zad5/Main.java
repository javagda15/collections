package pl.sda.collections.zad5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>(Arrays.asList(
                new Student("Marian", "Mariański", "123", Plec.KOBIETA),
                new Student("Guć", "Filip", "124", Plec.MĘZCZYZNA)
        ));

        //
        System.out.println(list);

        //
        for (Student student : list) {
            System.out.println(student);
        }

        System.out.println("Kobiety:");
        for (Student student : list) {
            if (student.getPlec() == Plec.KOBIETA) {
                System.out.println(student);
            }
        }

        System.out.println("Mężczyźni:");
        for (Student student : list) {
            if (student.getPlec() == Plec.MĘZCZYZNA) {
                System.out.println(student);
            }
        }

        System.out.println("Indeksy");
        for (Student student : list) {
            System.out.println(student.getPesel());
        }


    }
}
