package pl.sda.collections.zad5;

public class Student {
    private String name;
    private String surname;
    private String pesel;
    private Plec plec;

    public Student() {
    }

    public Student(String name, String surname, String pesel, Plec plec) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.plec = plec;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", plec=" + plec +
                '}';
    }
}
