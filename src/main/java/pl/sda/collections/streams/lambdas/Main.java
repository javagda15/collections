package pl.sda.collections.streams.lambdas;


import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Predicate<Integer> podzielna = l -> l % 2 == 0;

        Predicate<Integer> podzielnaDlugie = new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer % 2 == 0;
            }
        };

        Predicate<List<String>> listPredicate = strings -> {
            for (String tekst : strings) {
                if (tekst.contains("gotchya")) {
                    return true;
                }
            }
            return false;
        };

        Predicate<List<String>> predicate = new Predicate<List<String>>() {
            @Override
            public boolean test(List<String> strings) {
                for (String tekst : strings) {
                    if (tekst.contains("gotchya")) {
                        return true;
                    }
                }
                return false;
            }
        };

        Predicate<Person> personPredicate = p -> p.isMale();
        Predicate<Person> personPredicate1 = p -> p.getFirstName().equals("Jacek");
        Predicate<Person> personPredicate2 = p -> p.getAge() > 18;
        Predicate<List<Person>> personPredicate3 = list -> {
            for (Person p : list) {
                if (personPredicate2.test(p) && personPredicate1.test(p)) {
                    return true;
                }
//                if(p.getAge() >=18 && p.getFirstName().equals("Jacek")){
//                    return true;
//                }
            }
            return false;
        };
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);

        List<Person> personList = new ArrayList<>(Arrays.asList(
                person1,
                person2,
                person3,
                person4,
                person5,
                person6,
                person7,
                person8,
                person9
        ));

        //a
        List<Person> mezczyzni = personList.stream()
                .filter(p -> p.isMale())
                .collect(Collectors.toList());

        //b
        List<Person> dorosleKobiety = personList.stream()
                .filter(p -> p.getAge() > 18)
                .filter(p -> !p.isMale())
                .collect(Collectors.toList());

        // podpowiedź do C - na końcu nie ma collect, tylko po odfiltrowaniu
        //                  mamy użyć metodę findFirst / findAny
        //c
        Optional<Person> doroslyJacek = personList.stream()
                .filter(p -> p.getFirstName().equals("Jacek"))
                .filter(p -> p.getAge() > 18)
                .findFirst();

        //d
        List<String> listaNazwisk = personList.stream()
                .filter(p -> p.getAge() > 15 && p.getAge() < 19) // przedział wiekowy
                .map(p -> p.getLastName()) // zamieniam osoby na nazwiska
                .collect(Collectors.toList()); // zbieram wyniki

        //e
        int sum = personList.stream()
                .mapToInt(p -> p.getAge()).sum();

        //f
        OptionalDouble sredniaWieku = personList.stream()
                .filter(p -> p.isMale())
                .mapToInt(p -> p.getAge()).average();
        if (sredniaWieku.isPresent()) {
            System.out.println("Srednia " + sredniaWieku.getAsDouble());
        }

        //g
        Optional<Person> osoba = personList.stream()
                .sorted((o1, o2) -> {
                    return (o1.getAge() > o2.getAge() ? -1 : ((o1.getAge() < o2.getAge()) ? 1 : 0));
//                    if (o1.getAge() > o2.getAge()) {
//                        return 1;
//                    } else if (o1.getAge() < o2.getAge()) {
//                        return -1;
//                    }
//                    return 0;
                }).findFirst();

        System.out.println(osoba.get());

        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");

        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);

        List<Programmer> programmers = Arrays.asList(programmer1, programmer2, programmer3, programmer4,
                programmer5, programmer6, programmer7, programmer8, programmer9);


        // f

        Set<String> langs = programmers.stream()
                .map(p -> p.getLanguages())     // lista list
                .flatMap(Collection::stream)    // lista stringów
                .collect(Collectors.toSet());   // lista do setu

//        uzyskaj płeć programisty, który zna największą ilość języków
        Optional<Programmer> programmer = programmers.stream()
                .sorted((p1, p2) -> {
                    return (p1.getLanguages().size() > p2.getLanguages().size() ? -1 : ((p1.getLanguages().size() < p2.getLanguages().size()) ? 1 : 0));
                }).findFirst();
        boolean isMale = programmer.get().getPerson().isMale();

//        programmers.stream().map(p -> p.getLanguages()).flatMap(Collection::stream).collect(Collectors.toMap(p-> p, ))

//        zwroc listę językow posortowanych wedlug powszechnosci
        Map<String, Long> zliczamy = programmers.stream()
                .map(p -> p.getLanguages())
                .flatMap(List::stream)  // lista języków z powtórzeniami = java, c, c, c#, java ...
                .sorted()
                .collect(Collectors.groupingBy(p -> p, LinkedHashMap::new, Collectors.counting())) // mapa język -> ilość wystąpień
                .entrySet().stream()                                                        // rozbicie na entry set
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))              // sortuje po wartościach
//                .forEach(System.out::println);
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldVal, newVal) -> oldVal, LinkedHashMap::new));
        // kluczem pozostaje klucz
        // wartość dalej jest wartością
        // mapping wartość na wartość
        // zebranie linkedHasMap - zachowuje kolejność wstawiania
        System.out.println(zliczamy);

        List<String> byPopularity = programmers.stream().distinct().flatMap(
                x -> x.getLanguages().stream()).distinct().sorted((x, y) -> {
            long xProgrammers = programmers.stream()
                    .filter(a -> a.getLanguages().contains(x)).count();
            long yProgrammers = programmers.stream()
                    .filter(a -> a.getLanguages().contains(y)).count();
            return (int) xProgrammers - (int) yProgrammers;
        }).collect(Collectors.toList());
        System.out.println(byPopularity);
    }
}
